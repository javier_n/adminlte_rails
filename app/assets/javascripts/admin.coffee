# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# Morris.js charts
#= require raphael/raphael-min
#= require morris/morris.min

# Sparkline
#= require sparkline/jquery.sparkline.min

# jvectormap
#= require jvectormap/jquery-jvectormap-1.2.2.min
#= require jvectormap/jquery-jvectormap-world-mill-en

# jQuery Knob Chart
#= require knob/jquery.knob

# daterangepicker
#= require daterangepicker/moment.min
#= require daterangepicker/daterangepicker

# datepicker
#= require datepicker/bootstrap-datepicker

# Bootstrap WYSIHTML5
#= require bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min

# Slimscroll
#= require slimScroll/jquery.slimscroll.min

# FastClick
#= require fastclick/fastclick.min

# AdminLTE App
#= require admin_lte/pages/dashboard